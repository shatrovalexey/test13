package Test13 ;
=pod
=head1 Класс для тестирования подзадач задания
Запуск основной задачи класса:
* генерация журналов:
perl -Ilib -MTest13\ generate -e0
* загрузка сгенерированных журналов:
perl -Ilib -MTest13\ upload -e0
* запуск демона, отдающего данные для ExjJS Grid:
perl -Ilib -MTest13\ datasource -e0
=cut
use strict ;
use warnings ;

%{ + __PACKAGE__ . '::CMD' } = (
	'generate' => 'Generate' ,
	'upoad' => 'Upload' ,
	'datasource' => 'Datasource' ,
) ;

=pod
=head2 Метод import( $;$ )
Импорт пакета
Аргументы:
$cmd - команда. Возможные значения: 'generate', 'upload', 'datasource'.
=cut
sub import( $;$ ) {
	my ( $class , $cmd ) = @_ ;

	$class->__load( ${ "${class}::CMD" }{ $cmd } ) ;
}

=pod
=head2 Метод __load( $;$ )
Загрузка выбранного при импорте пакета и его выполнение
Аргументы:
$package - имя пакета.
=cut
sub __load( $$ ) {
	my ( $class , $package ) = @_ ;

	return +( ) unless defined( $package ) ;

	my $result = eval( << "." ) ;
use ${ 'class' }::${ 'package' } ;

${ 'class' }::${ 'package' }->__example( ) ;
.

	warn( $@ ) if $@ ;

	return $result ;
}

+ 1 ;