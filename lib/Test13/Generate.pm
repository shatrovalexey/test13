package Test13::Generate ;
=pod
=head1 Класс для генерации файлов журналов
Запуск основной задачи класса:
perl -Ilib -MTest13::Generate\ execute -e0
после запуска в log появятся сгенерированные файлы журналов
=cut
use parent qw(Test13::Settings) ;
use Digest::MD5 ;
use POSIX ;
use Text::CSV ;
+ 1 ;

=pod
=head2 Метод __example( $ )
Пример использования модуля
=cut
sub __example( $ ) {
	return `perl -Ilib -M${\__PACKAGE__}\\ execute -e0` ;
}

=pod
=head2 Метод import( $;$ )
Импорт модуля
Аргументы:
$cmd - команда импорта. Возможно значение "execute".
=cut
sub import( $;$ ) {
	my ( $class , $cmd ) = @_ ;

	return $class->new( )->execute( ) if $cmd eq 'execute' ;
}

=pod
=head2 Метод configure( $;$ )
Загрузка настроек из файла
Аргументы:
$file_name - имя файла настроек в формате JSON. Если не задан, используется значение по-умолчанию.
=cut
sub configure( $;$ ) {
	my ( $self , $file_name ) = @_ ;

	my $config = $self->SUPER::configure( $file_name ) ;
	my $generate = $self->SUPER::__configure( $self->{ 'config' }{ 'generate_file_name' } ) ;

=end
Совмещение хэш-массивов настроек
=cut

	$config->{ $_ } = $generate->{ $_ } foreach keys( %$generate ) ;

	return $config ;
}

=pod
=head2 Метод digest( $;$ )
Одностороннее ширование
Аргументы:
$string - строка для шифрования
=cut
sub digest( $;$ ) {
=end
Если не передана строка для шифрования, то используется произвольное значение
=cut
	push( @_ , rand( ) ) unless @_ > 1 ;

=end
Если не передана строка для шифрования, то используется произвольное значение
=cut
	shift( @_ )->__digest( )->md5_hex( shift( @_ ) ) ;
}

=pod
=head2 Метод __digest( $ ) - создание объекта шифрования
=cut
sub __digest( $ ) {
	shift( @_ )->__coalesce( 'digest' , sub {
		Digest::MD5->new( ) ;
	} ) ;
}

=pod
=head2 Метод ipv4( $ )
Генерация IPv4-адреса
=cut
sub ipv4( $ ) {
	my ( $self ) = @_ ;

	join( '.' , map int( rand( $self->{ 'config' }{ 'ip_range' } ) ) , 1 .. 4 ) ;
}

=pod
=head2 Метод user_agent( $ )
Выбор произвольного User-Agent из списка
=cut
sub user_agent( $ ) {
	my ( $self ) = @_ ;

	$self->{ 'config' }{ 'user_agent' }[ rand( @{ $self->{ 'config' }{ 'user_agent' } } ) ] ;
}

=pod
=head2 Метод os_name( $ )
Выбор произвольного названия ОС из списка
=cut
sub os_name( $ ) {
	my ( $self ) = @_ ;

	$self->{ 'config' }{ 'os_name' }[ rand( @{ $self->{ 'config' }{ 'os_name' } } ) ] ;
}

=pod
=head2 Метод datetime( $$ )
Генерация и форматирование даты и времени записи
Аргументы:
$position - количество секунд для смещения
=cut
sub datetime( $$ ) {
	my ( $self , $position ) = @_ ;
	my $time = time( ) - $self->{ 'config' }{ 'datetime_shift' } + $position ;

	&POSIX::strftime( $self->{ 'config' }{ 'datetime_format' } , localtime( $time ) ) ;
}

=pod
=head2 Метод uri( $ )
Генерация произвольного URI
=cut
sub uri( $ ) {
	my ( $self ) = @_ ;

	sprintf( $self->{ 'config' }{ 'url_format' } , $self->digest( ) ) ;
}

=pod
=head2 Метод __csv( $ )
Кодек формата CSV
=cut
sub __csv( $ ) {
	my ( $self ) = @_ ;

	$self->__coalesce( 'csv' , sub {
		Text::CSV->new( $self->{ 'config' }{ 'csv' } ) ;
	} ) ;
}

=pod
=head2 Метод execute( $ )
Форматированный вывод CSV
=cut

sub execute( $ ) {
	my ( $self ) = @_ ;
=pod
Создание файловых дескрипторов для записи журналов
=cut
	my ( $log1 , $log2 ) = map $self->mkfh( $_ , '>' ) , @{ $self->{ 'config' }{ 'log' } } ;

	my $csv = $self->__csv( ) ;
	my ( $i , $j , $ipv4 , %ipv4 ) ;

=pod
Генерация журналов
=cut

	for ( $i = $self->{ 'config' }{ 'ip_count' } ; $i >= 0 ; $i -- ) {
=pod
Запись в журнал 2
=cut
		$ipv4 = $self->ipv4( ) ;

		redo( ) if exists( $ipv4{ $ipv4 } ) ;

		$ipv4{ $ipv4 } = undef( ) ;

		$csv->print( $log2 , [
			$ipv4 , $self->user_agent( ) , $self->os_name( )
		] ) ;

=pod
Запись журнала 1
=cut
		for ( $j = $self->{ 'config' }{ 'ip_log_count' } ; $j >= 0 ; $j -- ) {
			$csv->print( $log1 , [
				$self->datetime( $j ) , $ipv4 , $self->uri( ) , $self->uri( )
			] ) ;
		}
	}

=pod
Закрытие дескрипторов журналов
=cut
	$_->close( ) foreach $log1 , $log2 ;
}

=pod
=head1 COPYRIGHT

Shatrov Aleksej L<mail@ashatrov.ru>
=cut
