package Test13::Settings ;
=pod
=head1 Класс с общими методами
=cut
use strict ;
use warnings ;
use JSON ;
use IO::Handle ;

=pod
=head2 Константа CONFIG_FILE_NAME
Файл настроек по-умолчанию
=cut
use constant 'CONFIG_FILE_NAME' => 'config/config.json' ;

+ 1 ;

=pod
=head2 Метод __json( $ )
кодек JSON
=cut
sub __json( $ ) { $_[ 0 ]->__coalesce( 'json' => sub { JSON->new( )->utf8( 0 ) } ) }

=pod
=head2 Метод new( $;% )
Создание объекта
Аргументы:
% - хэш-массив с вашими данными для хранения в объекте
=cut
sub new( $;% ) {
	my $class = shift( @_ ) ;
	my $self = bless( { @_ } , $class ) ;

	$self->configure( ) ;

	return $self ;
}

=pod
=head2 Метод mkfh( $$;$ )
Открытие файлового дескриптора с нужной кодировкой
Аргументы:
$file_name - имя файла
$mode - режим создания из core::open, если не задан, используется '<'.
=cut
sub mkfh( $$;$ ) {
	my ( $self , $file_name , $mode ) = @_ ;

	die( "$!\n$file_name" ) unless open( my $fh , $mode || '<' , $file_name ) ;
	$fh->binmode( $self->{ 'config' }{ 'io_charset' } ) ;

	return $fh ;
}

=pod
=head2 Метод __configure( $;$ )
Загрузка настроек
Аргументы:
$file_name - файл настроек в формате JSON. Если не задан, используется CONFIG_FILE_NAME
=cut
sub __configure( $;$ ) {
	my ( $self , $file_name ) = @_ ;

	$self->__eval( sub {
		$self->__json( )->decode( do {
			local $/ ;
			$self->mkfh( $file_name || $self->CONFIG_FILE_NAME( ) )->getlines( ) ;
		} ) ;
	} ) ;
}

=pod
=head2 Метод configure( $;$ )
Загрузка настроек
Аргументы:
$file_name - файл настроек в формате JSON. Если не задан, используется CONFIG_FILE_NAME
=cut
sub configure( $;$ ) {
	my ( $self , $file_name ) = @_ ;

	$self->__coalesce( 'config' , sub {
		$self->__configure( $file_name ) ;
	} ) ;
}

=pod
=head2 Метод __coalesce( $$\&;@ )
Проверка существования значения по ключу и заполнение значения результатом выполнения функции
Аргументы:
$key - ключ
$sub - функция
@ - аргументы, передаваемые в $sub
=cut
sub __coalesce( $$\&;@ ) {
	my ( $self , $key ) = splice( @_ , 0 , 2 ) ;

	return $self->{ $key } if exists( $self->{ $key } ) ;

	my $sub = shift( @_ ) ;

	$self->__eval( sub {
		$self->{ $key } = $sub->( $self , @_ ) ;
	} , @_ ) ;
}

=pod
=head2 Метод __eval( $\&;@ )
Вывод предупреждения, если при выполнении eval не произошло ошибки или возврат результата
Аргументы:
$sub - функция для получения результата
@ - аргументы для выполнения $sub
=cut
sub __eval( $\&;@ ) {
	my ( $self , $sub ) = splice( @_ , 0 , 2 ) ;

	return $_ foreach eval { $sub->( @_ ) } ;

	warn( $@ ) ;

	return +( ) ;
}

=pod
=head1 COPYRIGHT

Shatrov Aleksej L<mail@ashatrov.ru>
=cut
