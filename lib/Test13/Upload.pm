package Test13::Upload ;
=pod
=head1 Класс для загрузки файлов журналов в БД
Запуск основной задачи класса:
perl -Ilib -MTest13::Upload\ execute -e0
после запуска в БД появятся данные, согласно настройкам из config/config.json
=cut
use strict ;
use warnings ;
use parent qw(Test13::DBA) ;
+ 1 ;

=pod
=head2 Метод __example( $ )
Пример использования модуля
=cut
sub __example( $ ) {
	return `perl -Ilib -M${\__PACKAGE__}\\ execute -e0` ;
}

=pod
=head2 Метод import( $;$ )
Импорт модуля
Аргументы:
$cmd - команда импорта. Возможно значение "execute".
=cut
sub import( $;$ ) {
	my ( $class , $cmd ) = @_ ;

	return $class->new( )->execute( ) if $cmd eq 'execute' ;
}

=pod
=head2 Метод execute( $ )
Выполнение загрузки данных в БД из файлов
=cut
sub execute( $ ) {
	my ( $self ) = @_ ;

=pod
=head3 Выполнение скрипта загрузки
Очистка таблиц "log2" и "log1"
=cut
	$self->dbh( )->do( $_ )
		foreach @{ $self->{ 'config' }{ 'upload' }{ 'scripts' } } ;

=pod
=head3 Заполнение "log2"
Заполнение происходит из STDIN, чтобы обойти ограничения доступа к файлу.
=head3 Заполнение "log1"
Заполнение происходит из STDIN, чтобы обойти ограничения доступа к файлу.
=cut
	$self->__pg_copy_file( $_->{ 'sql' } , $self->{ 'config' }{ 'log' }[ $_->{ 'log_id' } ] )
		foreach @{ $self->{ 'config' }{ 'upload' }{ 'copy' } } ;
}

=pod
=head3 Метод __pg_copy_file( $$$ )
Загрузка данных из файла в PostgreSQL
=cut
sub __pg_copy_file( $$$ ) {
=pod
=head3 Переменная $sql
SQL-код для загрузки файла в таблицу
=head3 Переменная $file_name
Имя файла жарнала для загрузки
=head3 Переменная $inp
Дескриптор для чтения файла журнала
=head3 Переменная $dbh
Объект подключения к СУБД
=cut
	my ( $self , $sql , $file_name ) = @_ ;
	my $inp = $self->mkfh( $file_name ) ;
	my $dbh = $self->dbh( ) ;

=pod
=head3 Выполнение запроса к СУБД
=cut
	$dbh->do( $sql , undef( ) , $self->{ 'config' }{ 'csv' }{ 'sep_char' } ) ;

=pod
=head3 Отправка данных из файла для загрузки
=cut
	my $buff ;

	$dbh->pg_putcopydata( $buff )
		while $inp->read( $buff , $self->{ 'config' }{ 'buff_size' } ) ;

=pod
=head3 Закрытие файла и окончание загрузки в СУБД
=cut
	$inp->close( ) ;
	$dbh->pg_putcopyend( ) ;
}

=pod
=head1 COPYRIGHT

Shatrov Aleksej L<mail@ashatrov.ru>
=cut
