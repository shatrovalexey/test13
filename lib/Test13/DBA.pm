package Test13::DBA ;
=pod
=head1 Класс Test13::DBA
Добавляет возможность подключения к СУБД
=cut
use strict ;
use warnings ;
use parent qw(Test13::Settings) ;
use DBI ;
+ 1 ;

=pod
=head2 Метод dbh( $ )
Подключение к СУБД
=cut
sub dbh( $ ) {
	my ( $self ) = @_ ;

	return $self->{ 'dbh' } if ref( $self ) && exists( $self->{ 'dbh' } ) &&
									( $self->{ 'dbh' }->ping( ) > 0 ) ;

	$self->{ 'dbh' } = DBI->connect_cached( @{ $self->{ 'config' }{ 'dba' } }
					{ + qw(connection_string login password settings) } ) ;
}

=pod
=head1 COPYRIGHT

Shatrov Aleksej L<mail@ashatrov.ru>
=cut
