package Test13::Datasource ;
=pod
=head1 Источник данных для ExtJS Grid
Запуск основной задачи класса:
twiggy --server Starman -D -llib --listen :5012 -MTest13::Datasource -e'Test13::Datasource->build'
Если порт 5012 занят, укажите другой порт.
После запуска появится демон HTTP, который будет отдавать содержимое по запросу.
=cut
use strict ;
use warnings ;
use parent qw(Test13::DBA) ;
use Plack ;
use Plack::Request ;
use Plack::Builder ;
+ 1 ;

=pod
=head2 Метод __example( $ )
Пример использования модуля
=cut
sub __example( $ ) {
	return `twiggy -D -llib --listen :5012 -M${\__PACKAGE__} -e'${\__PACKAGE__}->build'` ;
}

=pod
=head2 Метод import( $;$ )
Импорт модуля
Аргументы:
$cmd - команда импорта. Возможно значение "execute".
=cut
sub import( $;$ ) {
	my ( $class , $cmd ) = @_ ;

	return $class->new( )->build( ) if $cmd eq 'execute' ;
}

=pod
=head2 Метод execute
Обработка запросов и вывода заголовков
Аргументы:
$env - данные окружения запроса Plack
=cut

sub execute( $$ ) {
	my ( $self , $env ) = @_ ;

	my $request = Plack::Request->new( $env ) ;
	my $args = $request->parameters ;

=pod
=head2 Переменная $page_current
Текущая страница
=cut
	my $page_current = $args->{ 'page' } || 0 ;

=pod
=head2 Переменная $page_size
Размер страницы в записях
=cut
	my ( $page_size ) = $args->{ 'limit' } || $self->{ 'config' }{ 'record_limit' } ;

=pod
=head2 Переменная $order_dir
Направление упорядочения результата с проверкой от инъекций
=cut
	my ( $order_dir ) = grep {
		uc( $args->{ 'dir' } ) eq $_ 
	} qw(ASC DESC) ;

	my %result = %{ $self->{ 'config' }{ 'result' } } ;

=pod
=head2 Переменная $order_by
Столбец упорядочения результата с проверкой от инъекций
=cut
	my ( $order_by ) = grep {
		$args->{ 'sort' } eq $_->{ 'name' } ;
	} @{ $result{ 'metaData' }{ 'default' } } ;

=pod
=head2 Переменная $record_current
Номер текущей записи в сущности
=cut
	my ( $record_current ) = $args->{ 'start' } || 0 ;

=pod
=head2 Переменная @sql_args
Аргументы для подстановки в SQL-код
=cut
	my @sql_args ;

=pod
=head2 Переменная $sql_code
SQL-код
=cut
	my @sql_code = ( << '.' , << '.' , << '.' ) ;
SELECT
.
	count( * ) AS "count"
.
FROM
	"v_log" AS "vl1"
.

=pod
Количество записей в сущности
=cut
	@result{ 'totalCount' } = $self->dbh( )->selectrow_array( join( '' , $sql_code ) ) ;

	$sql_code[ 1 ] = << '.' ;
	"vl1".*
.

	if ( defined( $order_by ) ) {
		push( @sql_code , << "." ) ;
ORDER BY
	"vl1"."$order_by"
.
		push( @sql_code , $order_dir ) if defined( $order_dir ) ;
	}

=pod
Ограничение количества записей в сущности для вывода
=cut
	push( @args , $page_current * $page_size , $page_size ) ;
	$sql_code .= << '.' ;
OFFSET ? LIMIT ?
.

=pod
Получение данных из СУБД
=cut
	if ( my $data = $self->dbh( )->selectall_hashref( $sql_code , 'ip' , undef( ) , @args ) ) {
		$result{ 'rows' } = [ values( %$data ) ] ;
	}

=pod
Вывод ответа от сервера
=cut
	$self->publish(
		'request' => $request ,
		'code' => $self->{ 'config' }{ 'http' }{ 'code' }{ 'ok' } ,
		'headers' => [ @{ $self->{ 'config' }{ 'http' } }{ + qw(ctype json charset) } , $self->{ 'config' }{ 'charset' } ] ,
		'body' => \%result
	) ;
}

=pod
=header2 Метод publish
Публикация ответа сервера
Аргументы - хэш-массив с ключами:
code - HTTP-код состояния
request - объект Plack::Request
headers - массив HTTP-заголовков ответа
body - тело ответа. Строка или ссылка на структуру данных для кодирования в JSON
=cut
sub publish( $% ) {
	my ( $self , %args ) = @_ ;

=pod
Если в body передана ссылка на структуру данных, то ссылку необходимо кодировать в формат JSON
=cut
	$args{ 'body' } = $self->__json( )->encode( delete( $args{ 'body' } ) )
			if ref( $args{ 'body' } ) ;

	my $response = $args{ 'request' }->new_response( $args{ 'code' } ) ;
	$response->header( @{ $args{ 'headers' } } ) ;
	$response->body( $args{ 'body' } ) ;
	$response->finalize( ) ;
}

=pod
=head2 Метод build
Построение веб-сервиса Plack
=cut
sub build( $ ) {
	my ( $class ) = @_ ;
	my $self = $class->new( ) ;

	builder {
		mount '/' => sub( $ ) {
			$self->execute( @_ ) ;
		}
	} ;
}

=pod
=head1 COPYRIGHT

Shatrov Aleksej L<mail@ashatrov.ru>
=cut
